const jwt = require('jsonwebtoken')
const Superadmin = require('../models/superadmin')

const superadminAuth = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ','')
        const decoded = jwt.verify(token, 'thisismynewcourse')
        const superadmin = await Superadmin.findOne({ _id: decoded._id, 'tokens.token': token })

        if (!superadmin) {
            throw new Error()
        }

        req.token = token
        req.superadmin = superadmin
        next()
    } catch (e) {
        res.status(401).send({ error: 'Please authenticate'})
    }
}

module.exports = superadminAuth