const jwt = require('jsonwebtoken')
const Referer = require('../models/referer')

const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ','')
        const decoded = jwt.verify(token, 'thisismynewcourse')
        const referer = await Referer.findOne({ _id: decoded._id, 'tokens.token': token })

        if (!referer) {
            throw new Error()
        }

        req.token = token
        req.referer = referer
        next()
    } catch (e) {
        res.status(401).send({ error: 'Please authenticate'})
    }
}

module.exports = auth