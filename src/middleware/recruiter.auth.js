const jwt = require('jsonwebtoken')
const Recruiter = require('../models/recruiter')

const recruiterAuth = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ','')
        const decoded = jwt.verify(token, 'thisismynewcourse')
        const recruiter = await Recruiter.findOne({ _id: decoded._id, 'tokens.token': token })

        if (!recruiter) {
            throw new Error()
        }

        req.token = token
        req.recruiter = recruiter
        next()
    } catch (e) {
        res.status(401).send({ error: 'Please authenticate'})
    }
}

module.exports = recruiterAuth