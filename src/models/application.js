const mongoose = require('mongoose')
const validator = require('validator')

const Application = mongoose.model('Application', {
    coverLetter: {
        type: String,
        default:""
    },
    CV: {
        type: String,
        
        default:""
    },
    name: {
        type: String,
        required: true,
        default:""
    },
    company: {
        type: String,
        default:""
    },
    jobTitle: {
        type: String,
        default:""
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        required: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid')
            }
        }
    },
    contactNumber: {
        type: String,
        default:""
    },
    comments: {
        type: String,
        default:""
    },
    state: {    //pending, rejected, interview, new candidate   When do these states change.
        type: String,
        default: 'new candidate'// might have to change this to new candidate... changed... was default 'pending'
    },
    skillsRating: {
        type: Array,
        default:[]
    },
    overallRating: {
        type: Number,
        default:0
    },
    refererID: {
        type: mongoose.Schema.Types.ObjectId
    },
    recruiterID: {
        type: mongoose.Schema.Types.ObjectId
    },
    jobID: {
        type: mongoose.Schema.Types.ObjectId
    },
    submitted: {    //what is this submitted?
        type: Boolean,
        default: false
    }, //find applicants. shared===true (only see applicants that have been shared by the referer)
    // new: {
    //     type: String,
    //     default: true
    // }, might not use this as for some reason it doesn't work
    newNumber: {  // what is newNuber??????
        type: Number,
        default: 1 //1 true //0 false
    }
})

module.exports = Application