const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const refererSchema = new mongoose.Schema({
    email: {
        type: String,
        trim: true,
        lowercase:true,
        unique:true,
        // required:true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid')
            }
        }
    },
    password: {
        type: String
        // required: true
    },
    name: {
        type: String
    },    
    surname: {
        type: String
    },
    type: {
        type: String
    },
    adminID: {
        type: mongoose.Schema.Types.ObjectId
    },
    tokens: [{   //what is this token?
        token: {
            type: String,
            required: true
        }
    }]
})

refererSchema.methods.generateAuthToken = async function () {
    const referer = this
    const token = jwt.sign({ _id: referer._id.toString() }, 'thisismynewcourse')

    referer.tokens = referer.tokens.concat({ token })
    await referer.save()

    return token
}

refererSchema.statics.findByCredentials = async (email, password) => {
    const referer = await Referer.findOne({ email })
    
    if (!referer) {
        throw new Error('unable to login')
    }
        
        const isMatch = await bcrypt.compare(password, referer.password)

    if (!isMatch) {
        throw new Error('unable to login')
    }

    return referer
}

//hash the plain text password
refererSchema.pre('save', async function (next) {
    const referer = this

    if (referer.isModified('password')) {
        referer.password = await bcrypt.hash(referer.password, 8)
    }

    next()
})

const Referer = mongoose.model('Referer', refererSchema)

module.exports = Referer

// refererSchema.methods.toJSON = function () {
//     const referer = this
//     const refererObject = referer.toObject()

//     delete refererObject.password
//     delete refererObject.tokens

//     return refererObject
// }