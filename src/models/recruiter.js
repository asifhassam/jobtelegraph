const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const recruiterSchema = new mongoose.Schema({
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        // required: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid')
            }
        }
    },
    password: {
        type: String
    },
    name: {
        type: String
    },
    surname: {
        type: String
    },
    type: {
        type: String
    },
    adminID: {
        type: mongoose.Schema.Types.ObjectId
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }]
})

recruiterSchema.methods.generateAuthToken = async function () {
    const recruiter = this
    const token = jwt.sign({ _id: recruiter._id.toString() }, 'thisismynewcourse')

    recruiter.tokens = recruiter.tokens.concat({ token })
    await recruiter.save()

    return token
}

recruiterSchema.statics.findByCredentials = async (email, password) => {
    const recruiter = await Recruiter.findOne({ email })
    
    if (!recruiter) {
        throw new Error('unable to login')
    }
    
    const isMatch = await bcrypt.compare(password, recruiter.password)

    if (!isMatch) {
        throw new Error('unable to login')
    }

    return recruiter
}


//hash the plain text password
recruiterSchema.pre('save', async function (next) {
    const recruiter = this

    if (recruiter.isModified('password')) {
        recruiter.password = await bcrypt.hash(recruiter.password, 8)
    }

    next()
})

const Recruiter = mongoose.model('Recruiter',recruiterSchema)

module.exports = Recruiter