const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const superadminSchema = new mongoose.Schema({
    email: {
        type: String,                ///same or diff as billingContactEmail/adminContactEmail? then remove
        required: true,
        trim: true,
        // unique: true,
        lowercase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid')
            }
        }
    },
    password: {
        type: String
        // required: true
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }]
})

superadminSchema.methods.generateAuthToken = async function () {
    const superadmin = this
    const token = jwt.sign({ _id: superadmin._id.toString() }, 'thisismynewcourse')

    superadmin.tokens = superadmin.tokens.concat({ token })
    await superadmin.save()

    return token
}

superadminSchema.statics.findByCredentials = async (email, password) => {
    const superadmin = await Superadmin.findOne({ email })
    
    if (!superadmin) {
        throw new Error('unable to login')
    }
        
    const isMatch = await bcrypt.compare(password, superadmin.password)

    if (!isMatch) {
        throw new Error('unable to login')
    }

    return superadmin
}

superadminSchema.pre('save', async function (next) {
    const superadmin = this

    if (superadmin.isModified('password')) {
        superadmin.password = await bcrypt.hash(superadmin.password, 8)
    }

    next()
})

const Superadmin = mongoose.model('Superadmin', superadminSchema)

module.exports = Superadmin