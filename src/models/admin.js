const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const adminSchema = new mongoose.Schema({
    email: {
        type: String,                ///same or diff as billingContactEmail/adminContactEmail? then remove
        required: true,
        trim: true,
        unique: true,
        lowercase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid')
            }
        }
    },
    name: {
        type: String                ///same as above
    },
    surname: {
        type: String
    },
    companyName: {
        type: String
    },
    companyCountry: {
        type: String
    },
    companyCity: {
        type: String
    },
    companyRegistrationNumber: {
        type: String
    },
    companyTaxNumber: {
        type: String
    },
    password: {
        type: String
        // required: true
    },
    verified: {
        type: Boolean,
        default: false
    },
    adminContactName: {
        type: String
    },
    adminContactEmail: {
        type: String
    },
    adminContactTel: {
        type: String
    },
    billingContactName: {
        type: String
    },
    billingContactEmail: {
        type: String
    },
    billingContactTel: {
        type: String
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }]
})

adminSchema.methods.generateAuthToken = async function () {
    const admin = this
    const token = jwt.sign({ _id: admin._id.toString() }, 'thisismynewcourse')

    admin.tokens = admin.tokens.concat({ token })
    await admin.save()

    return token
}

adminSchema.statics.findByCredentials = async (email, password) => {
    const admin = await Admin.findOne({ email })
    
    if (!admin) {
        throw new Error('unable to login')
    }
        
    const isMatch = await bcrypt.compare(password, admin.password)

    if (!isMatch) {
        throw new Error('unable to login')
    }

    return admin
}

adminSchema.pre('save', async function (next) {
    const admin = this

    if (admin.isModified('password')) {
        admin.password = await bcrypt.hash(admin.password, 8)
    }

    next()
})

const Admin = mongoose.model('Admin', adminSchema)

module.exports = Admin