const mongoose = require('mongoose')

const jobSchema = new mongoose.Schema({
    title: {
        type: String
    },
    level: {
        type: String
    },
    location: {
        type: String
    },
    contractType: {
        type: String
    },
    contractLength: {
        type: String
    },
    annualCostToCompany: {
        type: String
    },
    annualCostToCompanyRange: {
        type: String
    },
    remunerationVisibility: {
        type: String
    },
    postDate:{
        type: Date
    },

    closeDate: {
        type: Date //or String
    },
    skillsNeeded: {
        type: Array
    },
    aboutCompany:{
        type:String
    },
    description:{
        type:String
    },
    responsibilities:{
        type:String
    },
    basicqualification:{
        type:String
    },
    preferredqualification:{
        type:String
    },
    additionalinfo:{
        type:String
    },
    appointedhr:{
        type:String
    },
    appointedhremail:{
        type:String
    },
    finance:{
        type:String
    },
    financeemail:{
        type:String
    },

    recruiterID: {
        // type: String
        type: mongoose.Schema.Types.ObjectId

    },
    adminID: {
        // type: String
        type: mongoose.Schema.Types.ObjectId  //why do we need this??????

    }
})
// cooment

const Job = mongoose.model('Job', jobSchema)

module.exports = Job