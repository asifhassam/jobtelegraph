const express = require('express')
const Superadmin = require('../models/superadmin')
const Admin = require('../models/admin')
const Recruiter = require('../models/recruiter')
const Referer = require('../models/referer')
const superadminAuth = require('../middleware/superadmin.auth')
const router = new express.Router()

router.post('/superadmin/signup', async (req, res) => {
    try {
        const superadmin = await new Superadmin(req.body)
        const token = await superadmin.generateAuthToken()
        // await superadmin.save()
        res.status(201).send({superadmin,token})
    } catch (e) {
        res.status(500).send(e)
    }
})


//Logout admin
router.post('/superadmin/logout', superadminAuth, async (req, res) => {
    try {
        req.superadmin.tokens = req.superadmin.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.superadmin.save()

        res.send(req.superadmin)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.post('/superadmin/login', async (req, res) => {
    try {
        const superadmin = await Superadmin.findByCredentials(req.body.email, req.body.password)
        const token = await superadmin.generateAuthToken()
        res.send({superadmin, token})
    } catch (e) {
        res.status(400).send(e)
    }
})

router.get('/superadmin/all', superadminAuth, async (req, res) => {
    try {
        const admins = await Admin.find()
        res.send({admins})
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/superadmin/:id', superadminAuth, async (req, res) => {
    const adminId = req.params.id

    try {
        const adminReferers = await Referer.find({ adminID: adminId})
        const adminRecruiters = await Recruiter.find({ adminID: adminId})
        res.send({ adminReferers, adminRecruiters})
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/superadmin/referers/:id', superadminAuth, async (req, res) => {
    const adminId = req.params.id

    try {
        const adminReferers = await Referer.find({ adminID: adminId})
        res.send({ adminReferers})
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/superadmin/recruiters/:id', superadminAuth, async (req, res) => {
    const adminId = req.params.id

    try {
        const adminRecruiters = await Recruiter.find({ adminID: adminId})
        res.send({adminRecruiters})
    } catch (e) {
        res.status(500).send(e)
    }
})


router.post('/superadmin/changepass', superadminAuth, async (req, res) => {
    try {
        const superadmin = await Superadmin.findOne({ _id: req.superadmin._id })
        superadmin.password = req.body.password
        superadmin.save()
        res.send(superadmin)
    } catch (e) {
        res.status(500).send(e)
    }
})

module.exports = router