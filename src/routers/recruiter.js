const express = require('express')
const Recruiter = require('../models/recruiter')
const Application = require('../models/application')
const recruiterAuth = require('../middleware/recruiter.auth')
const Referer = require('../models/referer')
const Job = require('../models/jobs')
const {sendNewJobNotificationEmail} = require('../emails/account')
const router = new express.Router()

//Login Recruiter
router.post('/login/recruiter', async (req, res) => {
    try {
        const recruiter = await Recruiter.findByCredentials(req.body.email, req.body.password)
        const token = await recruiter.generateAuthToken()
        res.send({recruiter, token})
    } catch (e) {
        res.status(400).send(e)
    }
})

//Logout recruiter
router.post('/recruiter/logout', recruiterAuth, async (req, res) => {
    try {
        req.recruiter.tokens = req.recruiter.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.recruiter.save()

        res.send(req.recruiter)
    } catch (e) {
        res.status(500).send(e)
    }
})

// 

router.post('/recruiter/setJobs', recruiterAuth, async (req, res) => {
    try {
        const job = await new Job({
            ...req.body,
            postDate:new Date(),
            recruiterID: req.recruiter._id,
            adminID: req.recruiter.adminID
            // closeDate: new Date('')
        })
        
        /////send email to all referers with admin ID = req.recruiter.adminId
        const referers = await Referer.find({ adminID: req.recruiter.adminID})
        sendNewJobNotification(referers)
        //////
        
        await job.save()
        res.status(201).send(job)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/recruiter/jobs', recruiterAuth, async (req,res) => {
    try {
        const job = await Job.find({ recruiterID: req.recruiter._id })

        const count = await Job.countDocuments({ recruiterID: req.recruiter._id})
        //job array
        //for each job compare the two dates
        //console.log(job);
        checkActive(job).then(result=>{
            res.send(result);
        })
        
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/recruiter/job/:id', recruiterAuth, async (req,res) => {
    const jobId = req.params.id
    try {
        const job = await Job.find({ recruiterID: req.recruiter._id, _id:jobId })
       
        res.send(job)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/recruiter/applications/all', recruiterAuth, async (req, res) => {  //this also needs a job id
    try {
        const application = await Application.find({ recruiterID: req.recruiter._id, submitted: true })
        if (!application) {
            return res.status(404).send()
        } 
        res.send(application)
    } catch (e) {
        res.status(500).send(e)
    }
})

// router.get('/recruiter/applications/getreferernameandjobtitle/:refererid/:jobid', recruiterAuth, async (req, res) => {
//     const refererId = req.params.refererid
//     const jobId = req.params.jobid
//     try {
//         const referer = await Referer.findOne({ _id: refererId })
//         if (!referer) {
//             return res.status(404).send()
//         }

//         const job = await Job.findOne({ _id:jobId })
//         if (!job) {
//             return res.status(404).send()
//         }

//         res.send({refererName: referer.name, jobTitle: job.title})
//     } catch (e) {
//         res.status(500).send(e)
//     }
// })

router.get('/recruiter/applications/getreferernameandjobtitle', recruiterAuth, async (req,res) => {
    try {
        const application = await Application.find({ recruiterID: req.recruiter._id })
        addRefNameAndJobTitle(application).then(result=>{
            res.send(result);
        })
    } catch (e) {
        res.status(500).send(e)
    }
})



router.get('/recruiter/applications/new', recruiterAuth, async (req, res) => {
    try {
        // const application = await Application.find({ recruiterID: req.recruiter._id, submitted: true, new: true })
        const application = await Application.find({ recruiterID: req.recruiter._id, submitted: true, state:'rated' })
        if (!application) {
            return res.status(404).send()
        }
        const count = await Application.countDocuments({ recruiterID: req.recruiter._id, submitted:true, state:'rated' })
        res.send({application, count})
    } catch (e) {
        res.status(500).send(e)
    }
})


router.get('/recruiter/applications/:id', recruiterAuth, async (req, res) => {
    const applicationId = req.params.id
    try {
        const application = await Application.find({ _id: applicationId, submitted: true })
        if (!application) {
            return res.status(404).send()
        }
        res.send(application)
    } catch (e) {
        res.status(500).send(e)
    }
})

//change new candidate to rejected/fulfilled/whatever
router.patch('/recruiter/applications/:id', recruiterAuth, async (req, res) => {
    const applicationId = req.params.id
    try {
        const application = await Application.findOne({ _id: applicationId, submitted: true })
        if (!application) {
            return res.status(404).send()
        }
        application.state = req.body.state
        application.save()
        res.send(application)
    } catch (e) {
        res.status(500).send(e)
    }
})

function checkActive (jobsArray){
    return new Promise ((resolve,reject)=>{

        var newArr = [];
    for(i = 0;i < jobsArray.length;i++){
        
        var Active = jobsArray[i].closeDate > (new Date);
        
        console.log(jobsArray[0]);
        var obj = {
            ...jobsArray[i]._doc,
            Active:Active
        }
        newArr.push(obj);
       
    }
 
    resolve(newArr);

    })
    
    
    
}

// function addRefNameAndJobTitle(applicationsArray) {
//     return new Promise ((resolve,reject)=>{
//         var newArr = [];

//     for(i = 0;i < applicationsArray.length;i++){
//         let obj = {
//             ...applicationsArray[i]._doc
//         }

//         var refererID = applicationsArray[i].refererID
//         Referer.findOne({ _id: refererID }).then((referer) => {
//             console.log(referer.name)
//             obj.refererName = referer.name
//         }).catch((e) => {
//             res.status(500).send(e)
//         })
        
//         var jobID = applicationsArray[i].jobID
//         Job.findOne({ _id: jobID}).then((job) => {
//             console.log(job.title)
//             obj.jobTitle = job.title
//         }).catch((e) => {
//             res.status(500).send(e)
//         })
//         console.log(testArr)
//         newArr.push(obj);
//     }
//     resolve(newArr);
//     })
// }
async function addRefNameAndJobTitle(applicationsArray) {
    var newArr = [];

for(i = 0;i < applicationsArray.length;i++){
    var refererID = applicationsArray[i].refererID

    var referer = await Referer.findOne({ _id: refererID })
    var refererName = referer.email
    
    var jobID = applicationsArray[i].jobID
    var job = await Job.findOne({ _id: jobID})
    var jobTitle = job.title

    var obj = {
        ...applicationsArray[i]._doc,
        refererName,
        jobTitle
    }

    newArr.push(obj);
}
return newArr
}

async function sendNewJobNotification(referersArray) {
    for(i = 0;i < referersArray.length;i++){
        sendNewJobNotificationEmail(referersArray[i].email)
    }
    }






module.exports = router