const express = require('express')
const Referer = require('../models/referer')
const Job = require('../models/jobs')
const Application = require('../models/application')
const refererAuth = require('../middleware/referer.auth')
const {sendSuccessfullyReferredOrRejected} = require('../emails/account')
const router = new express.Router()

//Logout referer
router.post('/referer/logout', refererAuth, async (req, res) => {
    try {
        req.referer.tokens = req.referer.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.referer.save()

        res.send(req.referer)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.post('/login/referer', async (req, res) => {
    try {
        const referer = await Referer.findByCredentials(req.body.email, req.body.password)
        const token = await referer.generateAuthToken()
        res.send({referer, token})
    } catch (e) {
        res.status(400).send(e)
    }
})

router.get('/referer/jobs', refererAuth, async (req,res) => {
    try {
        const job = await Job.find({ adminID: req.referer.adminID })
        checkActive(job).then(result=>{
            res.send(result);
        })
        //res.send(job)
    } catch (e) {s
        res.status(500).send(e)
    }
})

router.get('/referer/job/:id',  async (req,res) => {
    const jobId = req.params.id
    try {
        const job = await Job.find({ _id:jobId })
        res.send(job[0])
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/referer/applications', refererAuth, async (req, res) => {
    try {
        const application = await Application.find({ refererID: req.referer._id })
        if (!application) {
            return res.status(404).send()
        }
        res.send(application)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/referer/applications/:jobid', refererAuth, async (req, res) => {
    try {
        const application = await Application.find({ refererID: req.referer._id,jobID:req.params.jobid,state:'new candidate'})
        if (!application) {
            return res.status(404).send()
        }
        res.send(application)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/referer/getdetails', refererAuth, async (req, res) => {
    try {
        
            res.send({refererID: req.referer._id});
        
        
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/referer/application/:id', refererAuth, async (req, res) => {
    const applicationId = req.params.id
    try {
        const application = await Application.find({ _id: applicationId })
        if (!application) {
            return res.status(404).send()
        }
        res.send(application[0])
    } catch (e) {
        res.status(500).send(e)
    }
})


//referer submit application to recruiter - sets rating only for now
router.post('/referer/applications/rate/:id', refererAuth, async (req, res) => {

    const applicationId = req.params.id
    const status   = req.body.status
    try {
        const application = await Application.findOne({ _id: applicationId })
        if (!application) {
            return res.status(404).send()
        }

        if(status == 'rejected'){
            application.state = 'rejected'
        }else{
        

        application.submitted = true //for issues to not fuck out

        application.state = 'rated'
        application.skillsRating = req.body.skillsRating// send an array as same length of the skills array
        application.overallRating = req.body.overallRating
        }
        
        await application.save()
        res.send(application)
       
    } catch (e) {
        res.status(500).send(e)
    }
})

//counts new documents(only works on patch)
router.patch('/referer/applications/countnew', refererAuth, async (req, res) => {
    try {
        const count = await Application.countDocuments({ newNumber:1, refererID: req.referer._id })
        res.send({count})
    } catch (e) {
        res.status(500).send(e)
    }
})

//changes 1 (new applications) to 0 (old)
router.patch('/referer/applications/nomorenew', refererAuth, async (req, res) => {
    try {
        const updated = await Application.updateMany({"newNumber": 1, refererID: req.referer._id}, {"$set":{"newNumber":0}})
        res.send({updated:updated.nModified})
    } catch (e) {
        res.status(500).send(e)
    }
})
//gives submitted application
router.get('/referer/applicationssubmitted', refererAuth, async (req, res) => {
    
    try {
        
        const application = await Application.find({ "submitted":true,refererID: req.referer._id })
        if (!application) {
            return res.status(404).send()
        }
        res.send(application)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.post('/referer/applications/:id/sendmail', refererAuth, async (req,res) => {
    const applicationId = req.params.id
    try {
        const application = await Application.findOne({ _id: applicationId })
        if (!application) {
            return res.status(404).send()
        }
        sendSuccessfullyReferredOrRejected(application.email, req.body.subject, req.body.text)
        res.send()
    } catch (e) {
        res.status(500).send(e)
    }
})

function checkActive (jobsArray){
    return new Promise((resolve,reject)=>{

        var newArr = [];
    for(i = 0;i < jobsArray.length;i++){
        
        var Active = jobsArray[i].closeDate > (new Date);
        
        //console.log(jobsArray[0]);
        var obj = {
            ...jobsArray[i]._doc,
            Active:Active
        }

       
  
        newArr.push(obj);
       
    }
 
    resolve(newArr);

    })
    //console.log(jobsArray);
    
    
}

module.exports = router
