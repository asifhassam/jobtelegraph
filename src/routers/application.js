const express = require('express')
const Job = require('../models/jobs')
const Application = require('../models/application')

const router = new express.Router()

router.post('/applications/signup/:jobID/:refererID', async (req,res) => {
    const jobID = req.params.jobID
    const refererID = req.params.refererID
    try {
        const job = await Job.findOne({ _id:jobID })
        
        if (!job) {
            return res.status(404).send()
        }
        
        const recruiterID = job.recruiterID.toString()
        
        const application = new Application({
            ...req.body,
            jobID,
            refererID,
            recruiterID
        })
        //console.log(application);
        await application.save()
        res.status(201).send(application)
    } catch (e) {
        console.log(e);
        res.status(500).send(e.message)
    }
})

module.exports = router