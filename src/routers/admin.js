const express = require('express')
const Admin = require('../models/admin')
const Recruiter = require('../models/recruiter')
const Referer = require('../models/referer')
const adminAuth = require('../middleware/admin.auth')
const {sendVerificationEmail, sendLogin} = require('../emails/account')
const router = new express.Router()
const generator = require('generate-password')

router.post('/signup', async (req, res) => {
    const admin = new Admin(req.body)
    try {
        await admin.save()
        sendVerificationEmail(admin._id,admin.email)
        res.status(201).send(admin)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.post('/signup/step1/:id', async (req, res) => {
    try {
        const admin = await Admin.findById(req.params.id)
        admin.password = req.body.password
        admin.name = req.body.name
        admin.surname = req.body.surname
        admin.verified = true
        admin.companyName = req.body.companyName
        await admin.save()
        res.send({admin})
    } catch (e) {
        res.status(500).send(e)
    }
})

router.post('/signup/step2/:id', async (req, res) => {
    try {
        const admin = await Admin.findById(req.params.id)
        admin.password = req.body.password
        admin.name = req.body.name
        admin.surname = req.body.surname
        admin.verified = true
        admin.companyName = req.body.companyName
        admin.companyCountry = req.body.companyCountry,
        admin.companyCity = req.body.companyCity,
        admin.companyRegistrationNumber = req.body.companyRegistrationNumber,
        admin.companyTaxNumber = req.body.companyTaxNumber
        const token = await admin.generateAuthToken()
        await admin.save()
        res.status(201).send({admin, token})
    } catch (e) {
        res.status(500).send(e)
    }
})

//Logout admin
router.post('/admin/logout', adminAuth, async (req, res) => {
    try {
        req.admin.tokens = req.admin.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.admin.save()

        res.send(req.admin)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.post('/login/admin', async (req, res) => {
    try {
        const admin = await Admin.findByCredentials(req.body.email, req.body.password)
        const token = await admin.generateAuthToken()
        res.send({admin, token})
    } catch (e) {
        res.status(400).send(e)
    }
})


//Sets New Members
// router.post('/admin/dashboard/setUsers', adminAuth, async (req,res) => {
//     try {
//         if (req.body.type === "recruiter") {
//             const recruiter = new Recruiter({
//                 name: req.body.name,
//                 surname: req.body.surname,
//                 email: req.body.email,                       
//                 password: 'password',                        
//                 adminID: req.admin._id
//             })
//             await recruiter.save()
//             res.status(201).send(recruiter)
//         }
//         if (req.body.type === "referer") {
//             const referer = new Referer({
//                 name: req.body.name,
//                 surname: req.body.surname,
//                 email: req.body.email,
//                 password: 'password',                        
//                 adminID: req.admin._id
//             })
//             await referer.save()
//             res.status(201).send(referer)
//         }
//     } catch (e) {
//         res.status(500).send(e)
//     }
// })

router.get('/admin/dashboard', adminAuth, async (req, res) => {
    try {
        const recruiters = await Recruiter.find({ adminID:req.admin._id })
        const referers = await Referer.find({ adminID:req.admin._id })

        res.send({ recruiters, referers})
    } catch (e) {
        res.status(500).send(e)
    }
})

router.post('/admin/dashboard/setUsers', adminAuth, async (req,res) => {
    try {
        const password = generator.generate({
            length: 10,
            numbers: true
        })
        if (req.body.type === "recruiter") {
            const recruiter = new Recruiter({
                name: req.body.name,
                surname: req.body.surname,
                email: req.body.email,                        ///above 3 lines can be shortened to ...req.body
                password: password,                        ///change this password to anything you like
                adminID: req.admin._id
            })
            await recruiter.save()
            sendLogin(req.body.email, req.body.type, password)
            res.status(201).send(recruiter)
        }
        if (req.body.type === "referer") {
            const referer = new Referer({
                name: req.body.name,
                surname: req.body.surname,
                email: req.body.email,
                password: password,                        ///change this password to anything you like
                adminID: req.admin._id
            })
            await referer.save()
            sendLogin(req.body.email, req.body.type, password)
            res.status(201).send(referer)
        }
    } catch (e) {
        res.status(500).send(e)
    }
})

module.exports = router