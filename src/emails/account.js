const sgMail = require('@sendgrid/mail')

sgMail.setApiKey('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')

const sendVerificationEmail = (adminId, adminEmail) => {
    sgMail.send({
        to: adminEmail,
        from: 'charles.sc.yang@gmail.com',
        subject: 'Thanks for joining in!',
        text: `Welcome to the JobTelegraph. Click the link to verify - http://job-telegraph.netlify.com/auth/complete-companyregistration/${adminId}`
    })
}

const sendLogin = (email, type, password) => {
    sgMail.send({
        to: email,
        from: 'charles.sc.yang@gmail.com',
        subject: 'Welcome to JobTelegraph',
        text: `Welcome to the app. Click the link to login as a ${type} - http://job-telegraph.netlify.com/auth/login
Sign in as ${type}
Your username is ${email}
Your password is ${password}`
    })
}

const sendSuccessfullyReferredOrRejected = (email, subject, text) => {
    sgMail.send({
        to: email,
        from: 'charles.sc.yang@gmail.com',
        subject: subject,
        text: text
    })
}

const sendNewJobNotificationEmail = (email) => {
    sgMail.send({
        to: email,
        from: 'charles.sc.yang@gmail.com',
        subject: 'Job Telegraph - New Job Available',
        text: `Hi there 
        We have a new vacancy and would love your help to find the perfect candidate. Please follow the 
        link to view the job ad and then share it to your personal network via social media, email and
        instant message. You can review, rate and refer any applicants from your personal network. If
        someone you refer gets the job you will earn a very welcome and sizable referal fee. 
        
        Click the link to login - http://job-telegraph.netlify.com/auth/login`
    })
}

module.exports = {
    sendVerificationEmail,
    sendLogin,
    sendSuccessfullyReferredOrRejected,
    sendNewJobNotificationEmail
}