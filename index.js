const express = require('express')
require('./src/db/mongoose')
const Applicant = require('./src/models/application')
const Referer = require('./src/models/referer')
const auth = require('./src/middleware/auth')

//require the routers
const adminRouter = require('./src/routers/admin')
const superadminRouter = require('./src/routers/superadmin')
const recruiterRouter = require('./src/routers/recruiter')
const refererRouter = require('./src/routers/referer')
const applicationRouter = require('./src/routers/application')

const cors = require('cors');


const app = express()
const port = process.env.PORT || 3000

app.use(cors());
app.options('*', cors());

app.use(express.json())
app.use(adminRouter)
app.use(recruiterRouter)
app.use(refererRouter)
app.use(superadminRouter)
app.use(applicationRouter)









// app.post('/referer/signup', async (req, res) => {
//     const referer = new Referer(req.body)

//     try {
//         await referer.save()
//         const token = await referer.generateAuthToken()
//         res.status(201).send({referer,token})
//     } catch (e) {
//         res.status(400).send(e)
//     }
// })

// app.post('/referer/login', async (req,res) => {
//     try {
//         const referer = await Referer.findByCredentials(req.body.email, req.body.password)
//         const token = await referer.generateAuthToken()
//         res.send({referer, token})
//     } catch (e) {
//         res.status(400).send()
//     }
// })

// app.post('/referer/logout', auth, async (req, res) => {
//     try {
//         req.referer.tokens = req.referer.tokens.filter((token) => {
//             return token.token !== req.token
//         })
//         await req.referer.save()

//         res.send()
//     } catch (e) {
//         res.status(500).send()
//     }
// })

// //View all applicants
// app.get('/referer/applications', auth, (req, res) => {
//     Applicant.find({ refererID: req.referer._id }).then((applicants) => {
//         res.send(applicants)
//     }).catch(() => {
//         res.status(500).send()
//     })
// })

// //Viewing an individual applicant
// app.get('/referer/applications/:id', auth, async (req, res) => {
//     const _id = req.params.id

//     try {
//         const applicant = await Applicant.findOne({ _id, refererID:req.referer._id })

//         if (!applicant) {
//             return res.status(404).send()
//         }

//         res.send(applicant)
//     } catch (e) {
//         res.status(500).send()
//     }
// })

//Applicant signup -signup on some website with refererID as a parameter
// app.post('/applications/signup/:jobID/:refererID', (req, res) => {
//     const jobID = req.params.jobID
//     const refererID = req.params.refererID
//     //then find job id and see who recruiter is
//     //const recruiterID = 
//     const applicant = new Applicant({
//         ...req.body,
//         jobID,
//         //recruiterID
//     })
    
//     applicant.save().then(() => {
//         res.status(201).send(applicant)
//     }).catch((e) => {
//         res.status(401).send('unable to signup')
//     })
// })

// //referer updating individual applications such as state (e.g. rejected, accepted)
// app.patch('/referer/applications/:id', auth, async (req, res) => {
//     const updates = Object.keys(req.body)
//     const allowedUpdates = ['name','email','state']
//     const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

//     if (!isValidOperation) {
//         return res.status(400).send({ error: 'Invalid updates' })
//     }

//     try {
//         const applicant = await Applicant.findOne({ _id:req.params.id, refererID:req.referer._id})

//         if (!applicant) {
//             return res.status(404).send()
//         }
        
//         updates.forEach((update) => applicant[update] = req.body[update])
//         await applicant.save()
    
//         res.send(applicant)
//     } catch (e) {
//         res.status(500).send(e)
//     }
// })

app.listen(port, () => {
    console.log('Server is up on port ' + port)
})